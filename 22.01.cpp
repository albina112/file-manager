﻿#include <iostream>
#include <filesystem>
#include <Windows.h>
#include <conio.h>

enum Color
{
	Black,
	DarkBlue,
	DarkGreen,
	DarkAqua,
	DarkRed,
	DarkPurple,
	DarkYellow,
	DarkWhite,
	Gray,
	Blue,
	Green,
	Aqua,
	Red,
	Purple,
	Yellow,
	White
};

struct Position
{
	decltype(COORD::X) x;
	decltype(COORD::Y) y;
};

struct ColorSet
{
	Color foregraund;
	Color background;
};

enum class Key
{
	None,
	W,
	S,
	Enter,
	Backspace,
	Delete,
	Size,
	DeleteAll,
	Esc
};

enum class ErrorCode
{
	InvalidPath
};

enum class Direction
{
	Up,
	Down
};

enum class Status
{
	Ok,
	Stop
};

using ContainerT = std::vector< std::string >;

struct FolderData
{
	ContainerT container;
	size_t carret;
	std::filesystem::path path;
};

enum class Action
{
	TryToOpen,
	ReturnToPrevFolder,
	Delete,
	PrintSize,
	DeleteAllWithExtension
};

void setCursorPosition(SHORT col, SHORT row)
{
	COORD coord = { col, row };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void setCursorPosition(Position position)
{
	setCursorPosition(position.x, position.y);
}

void setCursorColor(Color foregraund, Color background = Color::Black)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle, background << 4 | foregraund);
}

void setCursorColor(ColorSet color)
{
	setCursorColor(color.foregraund, color.background);
}

void showConsoleCursor(bool showFlag = true)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag;
	SetConsoleCursorInfo(out, &cursorInfo);
}

ContainerT getDataFromPath(std::filesystem::path path)
{
	namespace fs = std::filesystem;
	ContainerT folderData;

	if(!fs::exists(path))
		throw ErrorCode::InvalidPath;

	for (auto& data : fs::directory_iterator(path))
	{
		folderData.push_back(data.path().filename().string());
	}

	return folderData;
}

void printFolderData(const FolderData& folderData,
					 ColorSet selectColor = { Black, DarkAqua }, ColorSet defaultColor = { White, Black })
{
	for (auto it = folderData.container.begin(); it != folderData.container.end(); ++it)
	{
		auto distance = std::distance(folderData.container.begin(), it);

		if (distance == folderData.carret)
			setCursorColor(selectColor);

		std::cout << *it << std::endl;

		if (distance == folderData.carret)
			setCursorColor(defaultColor);
	}
}

Key readKey()
{
	int key = _getch();

	if (isalpha(key))
	{
		key = toupper(key);
	}

	if (key == 'W') return Key::W;
	if (key == 'S') return Key::S;
	if (key == '\r') return Key::Enter;
	if (key == '\b') return Key::Backspace;
	if (key == 'D') return Key::Delete;
	if (key == 'F') return Key::Size;
	if (key == 'A') return Key::DeleteAll;
	if (key == 27) return Key::Esc;

	return Key::None;
}

void moveSelectedRow(FolderData& folderData, Direction direction)
{
	auto tmp = static_cast<int>(folderData.carret);

	if (direction == Direction::Up)
		tmp--;
	else if (direction == Direction::Down)
		tmp++;

	auto folderSize = static_cast<int>(folderData.container.size());

	if (tmp < 0)
		tmp = folderSize - 1;
	if (tmp >= folderSize)
		tmp = 0;

	folderData.carret = tmp;
}

void printSize(FolderData& folderData)
{
	system("cls");
	std::filesystem::path path = folderData.path.string() + "\\" + folderData.container[folderData.carret];

	if (std::filesystem::is_directory(path))
	{
		std::cout << "Is directory" << std::endl;
		system("pause");
		return;
	}

	std::cout << "Size of " << folderData.container[folderData.carret] << " is: "
		<< (std::filesystem::file_size(path))
		<< "bytes" << std::endl;

	system("pause");
}

void deleteAllWithExtension(FolderData& folderData)
{
	std::string extension;
	std::cout << "Enter extension: ";
	std::getline(std::cin, extension);
	extension = "." + extension;
	for (auto const& iter : std::filesystem::recursive_directory_iterator(folderData.path))
	{
		if(iter.path().extension() == extension)
			std::filesystem::remove(iter);
	}
}

bool tryToOpen(FolderData& folderData)
{
	std::filesystem::path path = folderData.path.string() + "\\" + folderData.container[folderData.carret];
	if (std::filesystem::is_directory(path))
	{
		if (std::filesystem::exists(path))
		{
			folderData.path = path;
			return true;
		}
	}

	return false;
}

bool actionOnSelectedRow(FolderData& folderData, Action action)
{
	switch (action)
	{
	case Action::TryToOpen:
		return tryToOpen(folderData);
		break;
	case Action::ReturnToPrevFolder:
		folderData.path = folderData.path.parent_path();
		break;
	case Action::Delete:
		std::filesystem::remove_all(folderData.path.string() + "\\" + folderData.container[folderData.carret]);
		break;
	case Action::PrintSize:
		printSize(folderData);
		break;
	case Action::DeleteAllWithExtension:
		deleteAllWithExtension(folderData);
		break;
	default:
		break;
	}

	return false;
}

Status keyEvent(FolderData& folderData, Key key)
{
	if (key == Key::W)
		moveSelectedRow(folderData, Direction::Up);
	if (key == Key::S)
		moveSelectedRow(folderData, Direction::Down);
	if (key == Key::Enter)
	{
		if (actionOnSelectedRow(folderData, Action::TryToOpen))
		{
			folderData.container = getDataFromPath(folderData.path);
			folderData.carret = 0u;
		}
	}
	if (key == Key::Backspace)
	{
		actionOnSelectedRow(folderData, Action::ReturnToPrevFolder);
		folderData.container = getDataFromPath(folderData.path);
		folderData.carret = 0u;
	}
	if (key == Key::Delete)
	{
		actionOnSelectedRow(folderData, Action::Delete);
		folderData.container = getDataFromPath(folderData.path);
		folderData.carret = 0u;
	}
	if (key == Key::Size)
	{
		actionOnSelectedRow(folderData, Action::PrintSize);
		folderData.carret = 0u;
	}
	if (key == Key::DeleteAll)
	{
		actionOnSelectedRow(folderData, Action::DeleteAllWithExtension);
		folderData.container = getDataFromPath(folderData.path);
		folderData.carret = 0u;
	}
	if (key == Key::Esc)
		return Status::Stop;
	return Status::Ok;
}

const char* getMessageById(ErrorCode id)
{
	if (id == ErrorCode::InvalidPath)
		return  "Invalid path";
	return "Invalid error code";
}

int main()
{
	try
	{
		namespace fs = std::filesystem;
		showConsoleCursor(false);
		FolderData folderData = { getDataFromPath(fs::current_path()), 0u, fs::current_path() };

		while (true)
		{
			system("cls");
			printFolderData(folderData);

			if (keyEvent(folderData, readKey()) == Status::Stop)
				break;
		}
	}
	catch (ErrorCode id)
	{
		MessageBoxA(nullptr, getMessageById(id), "Error", MB_OK);
	}

	return 0;
}